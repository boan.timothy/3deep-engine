#include "Application.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>
#include <glm\ext.hpp>
#include <Gizmos.h>
#include <vld.h>

#include <iostream>

#include "Window.h"
#include "Interface.h"
#include "FlyCamera.h"
#include "Terrain.h"
#include "Timer.h"
#include "Physics.h"
#include "Model.h"

Application::Application() : Engine()
{

}

bool Application::Startup()
{
	// Set up Interface
	Interface_Startup();

	Gizmos::create();

	// Set up main camera
	camera = new FlyCamera();
	camera->SetInputWindow(window->GetWindow());
	glm::vec2 display = window->GetDisplaySize();
	camera->SetPerspective(glm::pi<float>() * 0.25f, display.x / display.y, 0.1f, 1000.0f);
	camera->SetLookAt(glm::vec3(0, 15, -15), glm::vec3(0, 10, 0), glm::vec3(0, 1, 0));
	//glm::vec3(-30, 15, -11)

	clearColor = glm::vec3(0.55f, 0.7f, 0.9f);
	glClearColor(clearColor.r, clearColor.g, clearColor.b, 1);
	cameraSpeed = camera->GetFlySpeed();

	// Set up terrain
	terrainGridSize = 1024;
	terrainOctaves = 6;
	terrainAmplitude = 3.0f;
	terrainPersistence = 0.3f;
	terrainFrequency = 0.05f;
	terrainSeed = 0;

	physics = new glen::Physics();
	physics->Startup();
	physics->SetUpVisualDebugger();
	physics->SetUpDynamicObjects();
	physics->SetUpPlayerController();
	physics->SetUpFluidDynamics();

	terrain = new Terrain();
	terrain->GenerateTerrain(terrainGridSize, terrainGridSize, 1.0f, 0, 0);
	terrain->GeneratePerlinNoise(terrainOctaves, terrainAmplitude, terrainPersistence, terrainFrequency, terrainSeed);
	terrain->GenerateShaders();
	terrain->GenerateHeightMap(physics);

	fbx = new glen::FBXModel("models/soulspear/soulspear.fbx");

	return true;
}

bool Application::Update(float deltaTime)
{	
	Interface_Update();
	
	physics->Update(deltaTime, window->GetWindow());

	camera->Update(deltaTime);

	return true;
}

void Application::Render()
{
	terrain->Render(camera);

	Gizmos::draw(camera->GetProjectionView());

	fbx->Render(camera);
	
	Interface_Render();

}

void Application::Shutdown()
{
	// This stuff should be better guarded so that systems can be disabled.
	// For the purpose of this assessment where things aren't likely to be changed
	// a lot it's functional, but hardly ideal. The same could go for many other areas
	// of the project, but time constraints mean that little things like defensive
	// programming aren't important, right? Right...?

	// TODO: as stated above, better defensive programming! It's VERY important!

	//delete fbx;
	
	if (camera != nullptr)
		delete camera;

	if (terrain != nullptr)
		delete terrain;

	Interface_Shutdown();

	physics->Shutdown();
	delete physics;

	Gizmos::destroy();
}

// ############################################################################
// ## INTERFACE METHODS #######################################################
// ############################################################################

bool Application::Interface_Startup()
{
	ImGui_ImplGlfwGL3_Init(window->GetWindow(), true);

	Interface_Resize();

	return true;
}

bool Application::Interface_Update()
{
	// ImGui is gross..

	ImGui_ImplGlfwGL3_NewFrame();
	
	ImGuiIO& io = ImGui::GetIO();

	ImGui::SetNextWindowPos(ImVec2(0, 20));
	ImGui::SetNextWindowSize(ImVec2(300, io.DisplaySize.y));
	
	ImGui::Begin("Debug Window");

	// This is bad! Cache this stuff! You're wasting function calls!
	ImGui::Text("Renderer Info");
	ImGui::Separator();
	char information[64];
	sprintf_s(information, 64, "%s%i.%i", "OpenGL Version: ", ogl_GetMajorVersion(), ogl_GetMinorVersion());
	ImGui::Text(information);
	sprintf_s(information, 64, "%s%s", "GLSL Version: ", glGetString(GL_SHADING_LANGUAGE_VERSION));
	ImGui::Text(information);
	sprintf_s(information, 64, "%s%s", "Vendor: ", glGetString(GL_VENDOR));
	ImGui::Text(information);
	sprintf_s(information, 64, "%s%s", "Renderer: ", glGetString(GL_RENDERER));
	ImGui::Text(information);

	// MC Hammer says: "Can't cache this!"
	char framerate[64];
	sprintf_s(framerate, 64, "%s%i", "Framerate: ", (int)(1 / timer->GetDeltaTime()));
	ImGui::Text(framerate);


	ImGui::Separator();
	ImGui::Text("Properties");
	ImGui::Separator();
	if (ImGui::ColorEdit3("clear color", glm::value_ptr(clearColor)))
	{
		glClearColor(clearColor.r, clearColor.g, clearColor.b, 1);
	}

	ImGui::Separator();
	ImGui::Text("Terrain Properties");
	ImGui::Separator();
	if (ImGui::DragInt("grid size", &terrainGridSize, 1.0f, 0, 2048))
	{

	}
	if (ImGui::DragInt("octaves", &terrainOctaves, 1.0f, 0, 8))
	{

	}
	if (ImGui::DragFloat("amplitude", &terrainAmplitude, 0.05f, 0.5f, 3.0f))
	{
		
	}
	if (ImGui::DragFloat("persistence", &terrainPersistence, 0.05f, 0.1f, 1.0f))
	{

	}
	if (ImGui::DragFloat("frequency", &terrainFrequency, 0.001f, 0.01f, 0.10f))
	{

	}
	if (ImGui::DragInt("seed", &terrainSeed, 1.0f, 0, INT_MAX))
	{

	}
	if (ImGui::Button("Regenerate"))
	{
		terrain->GenerateTerrain(terrainGridSize, terrainGridSize, 1.0f, 0, 0); // takes a short while as terrain size increases
		terrain->GeneratePerlinNoise(terrainOctaves, terrainAmplitude, terrainPersistence, terrainFrequency, terrainSeed); // takes a long while as terrain size increases
		//terrain->GenerateShaders(); // takes very little time, but doesn't need to be done
		terrain->GenerateHeightMap(physics);
	}


	ImGui::Separator();
	ImGui::Text("Camera Properties");
	ImGui::Separator();
	glm::vec3 camPos = camera->GetPosition();
	if (ImGui::DragFloat3("cam position", glm::value_ptr(camPos)))
	{
		camera->SetPosition(camPos);
	}
	float camSpeed = camera->GetFlySpeed();
	if (ImGui::DragFloat("cam speed", &camSpeed, 1.0f, 0.0f, 1000.0f))
	{
		camera->SetFlySpeed(camSpeed);
	}


	ImGui::Separator();
	ImGui::Text("FBX Light Properties");
	ImGui::Separator();
	glm::vec3 lightDir = fbx->GetLightDirection();
	if (ImGui::DragFloat3("direction", glm::value_ptr(lightDir), 0.05f))
	{
		fbx->SetLightDirection(lightDir);
	}
	glm::vec4 lightColor = fbx->GetLightColor();
	if (ImGui::ColorEdit4("light color", glm::value_ptr(lightColor)))
	{
		fbx->SetLightColor(lightColor);
	}
	float specularPower = fbx->GetSpecularPower();
	if (ImGui::DragFloat("specular power", &specularPower, 0.2f, 0.0f, 20.0f))
	{
		fbx->SetSpecularPower(specularPower);
	}

	ImGui::End();

	return true;
}

void Application::Interface_Render()
{
	ImGui::Render();
}

void Application::Interface_Shutdown()
{
	ImGui_ImplGlfwGL3_Shutdown();
}

void Application::Interface_Resize()
{
	ImGuiIO& io = ImGui::GetIO();
	glm::ivec2 windowSize = window->GetDisplaySize();
	io.DisplaySize.x = (float)windowSize.x;
	io.DisplaySize.y = (float)windowSize.y;
}

void Application::Interface_ShowMainMenuBar()
{
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("Exit", "Esc")) {}
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}
}

// ############################################################################
// ############################################################################