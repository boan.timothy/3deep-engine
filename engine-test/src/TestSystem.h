#pragma once

#include "System.h"

// Some test systems to try the messagebus that were abandoned to work
// on more pressing things. They'll be brought back, in time.

class TestSystem1 : glen::System
{
	TestSystem1();
	~TestSystem1();

	void HandleMessage(glen::Message* msg) override;
};

class TestSystem2 : glen::System
{
	TestSystem2();
	~TestSystem2();

	void HandleMessage(glen::Message* msg) override;
};