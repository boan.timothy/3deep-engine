#include "Terrain.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

#include <glm\vec2.hpp>
#include <glm\vec3.hpp>
#include <glm\ext.hpp>

#include "Shader.h"
#include "Texture.h"
#include "BaseCamera.h"
#include "Noise.h"
#include "Physics.h"

#include <vector>
#include <iostream>
#include <cassert>

Terrain::Terrain()
{
	// Generate our shader and load the textures for our terrain.
	shader = new glen::Shader();
	textures.push_back(new glen::Texture("textures/water.tga"));
	textures.push_back(new glen::Texture("textures/sand_03.tga"));
	textures.push_back(new glen::Texture("textures/grass_01.tga"));
	textures.push_back(new glen::Texture("textures/grass_rocks.tga"));
	textures.push_back(new glen::Texture("textures/rocks_grass.tga"));
	textures.push_back(new glen::Texture("textures/snow.tga"));

	heightMap = nullptr;
}

Terrain::~Terrain()
{
	if (shader != nullptr)
		delete shader;

	for (auto& texture : textures)
		delete texture;

	if (heightMap != nullptr)
		heightMap->release();

	textures.clear();
}

bool Terrain::GenerateTerrain(unsigned int rowCount, unsigned int columnCount, float scale, unsigned int xOrigin, unsigned int yOrigin)
{
	// Generate the vertices for the grid
	const unsigned int vertexCount = rowCount * columnCount;
	gridVertexCount = vertexCount;
	gridRows = rowCount;
	gridColumns = columnCount;
	
	vertices.resize(vertexCount);

	float width = columnCount * scale;
	float height = columnCount * scale;
	float halfWidth = width / 2.0f;
	float halfHeight = height / 2.0f;
	const glm::vec3 originPos(xOrigin - halfWidth, 0, yOrigin - halfHeight);

	for (unsigned int rowIndex = 0; rowIndex < rowCount; rowIndex++ )
	{
		for (unsigned int columnIndex = 0; columnIndex < columnCount; columnIndex++)
		{
			int index = rowIndex * columnCount + columnIndex;

			float columnPos = (float)columnIndex * scale;
			float rowPos = (float)rowIndex * scale;
			glm::vec3 pos(columnPos, 0, rowPos);

			vertices[index].position = glm::vec4((originPos + pos), 1);
			vertices[index].uv = glm::vec2(rowIndex / (float)rowCount, columnIndex / (float)columnCount);
			vertices[index].texuv = glm::vec2(rowIndex / 3.0f, columnIndex / 3.0f);
		}
	}

	// Create the indices
	const unsigned int indexCount = (rowCount - 1) * (columnCount - 1) * 6;
	gridIndexCount = indexCount;
	indices.resize(indexCount);

	unsigned int index = 0;
	for (unsigned int rowIndex = 0; rowIndex < (rowCount - 1); rowIndex++)
	{
		for (unsigned int columnIndex = 0; columnIndex < (columnCount - 1); columnIndex++)
		{
			unsigned int indexCurrent = rowIndex * columnCount + columnIndex;
			unsigned int indexNextRow = (rowIndex + 1) * columnCount + columnIndex;
			unsigned int indexNextColumn = indexCurrent + 1;
			unsigned int indexNextRowNextColumn = indexNextRow + 1;

			assert(index + 6 <= indexCount);
			// Triangle 1
			indices[index++] = indexCurrent;
			indices[index++] = indexNextRow;
			indices[index++] = indexNextRowNextColumn;

			// Triangle 2
			indices[index++] = indexCurrent;
			indices[index++] = indexNextRowNextColumn;
			indices[index++] = indexNextColumn;
		}
	}

	// More OpenGL stuff that should be in its own class, but it'll do for now.
	glGenBuffers(1, &vao);
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &ibo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, (rowCount * columnCount) * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec4)));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec4) + sizeof(glm::vec2)));
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);	

	return true;
}

bool Terrain::GenerateShaders()
{
	// Load and link the shaders for our terrain. In this case we want to blend six textures.
	// It's quite possible that the .vert shader here could be one file instead of one per .frag.
	if (shader->LoadVertexShader  ("shaders/terrain_texture_six.vert") &&
		shader->LoadFragmentShader("shaders/terrain_texture_six.frag"))
	{
		shader->LinkShaders();
		return true;
	}
	return false;
}

bool Terrain::GeneratePerlinNoise(int octaves, float amplitude, float persistence, float frequency, unsigned int seed)
{
	// I don't think resize(0) deletes the data, so I can't
	// remember what I was trying to achieve with this one.
	perlinData.resize(0);
	perlinData.resize(gridVertexCount);

	float scale = (1.0f / gridColumns) * 3;

	// Create an instance of our noise generator
	glen::PerlinNoise noise = glen::PerlinNoise(amplitude, frequency, persistence, octaves, seed);

	for (unsigned int x = 0; x < gridRows; ++x)
	{
		for (unsigned int y = 0; y < gridColumns; ++y)
		{
			// Old, tutorial style perlin noise. This has been left here in case we go and break
			// the custom noise generator we've made.

			/*float ampl = amplitude;
			float pers = persistence;
			perlinData[y * gridRows + x] = 0;
			float frequency = 1.0f;

			for (int o = 0; o < octaves; ++o)
			{
				frequency = powf(2, (float)o);
				float perlinSample = glm::perlin(vertices[y * gridColumns + x].position * scale * frequency) * 0.5f + 0.5f;
				perlinData[y * gridColumns + x] += perlinSample * ampl;
				ampl *= pers;
			}*/

			perlinData[y * gridColumns + x] += noise.GetHeight((float)x, (float)y);
		}
	}

	glGenTextures(1, &perlinTexture);
	glBindTexture(GL_TEXTURE_2D, perlinTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, gridRows, gridColumns, 0, GL_RED, GL_FLOAT, perlinData.data());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	return true;
}

void Terrain::GenerateHeightMap(glen::Physics* physics)
{
	// Create a rough height map. This relies on PhysX.

	if (heightMap != nullptr)
		physics->GetScene()->removeActor(*heightMap);

	heightMap = physics->CreateHeightMap(gridRows, gridColumns, perlinData.data(), 25);
	physics->GetScene()->addActor(*heightMap);
}

void Terrain::Render(BaseCamera* camera)
{
	// Not pretty but it does the job.
	// Ideally you'd want some kind of loop to render this stuff
	// if you have a different number of textures.

	unsigned int program = shader->GetShaderProgram();
	glUseProgram(program);
	unsigned int uniformLocation = glGetUniformLocation(program, "ProjectionView");
	glUniformMatrix4fv(uniformLocation, 1, false, glm::value_ptr(camera->GetProjectionView()));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, perlinTexture);
	uniformLocation = glGetUniformLocation(program, "PerlinTexture");
	glUniform1i(uniformLocation, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textures[0]->GetTexture());
	uniformLocation = glGetUniformLocation(program, "Texture0");
	glUniform1i(uniformLocation, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, textures[1]->GetTexture());
	uniformLocation = glGetUniformLocation(program, "Texture1");
	glUniform1i(uniformLocation, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, textures[2]->GetTexture());
	uniformLocation = glGetUniformLocation(program, "Texture2");
	glUniform1i(uniformLocation, 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, textures[3]->GetTexture());
	uniformLocation = glGetUniformLocation(program, "Texture3");
	glUniform1i(uniformLocation, 4);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, textures[4]->GetTexture());
	uniformLocation = glGetUniformLocation(program, "Texture4");
	glUniform1i(uniformLocation, 5);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, textures[5]->GetTexture());
	uniformLocation = glGetUniformLocation(program, "Texture5");
	glUniform1i(uniformLocation, 6);

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, gridIndexCount, GL_UNSIGNED_INT, 0);
	glUseProgram(0);
}