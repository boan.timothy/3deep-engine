#include "Interface.h"

#include <glm\ext.hpp>

#include "Gui.h"
#include "Window.h"
#include "Application.h"

Interface::Interface(Window* window) : Gui(window)
{
}

// ew, ew, yuck, ew, no, please no...
bool Interface::Update(Application* app)
{
	ImGuiIO& io = ImGui::GetIO();
	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::SetNextWindowSize(ImVec2(300, io.DisplaySize.y));
	ImGui::Begin("debug");
	ImGui::ColorEdit3("clear color", glm::value_ptr(app->clearColor));
	ImGui::End();

	return true;
}