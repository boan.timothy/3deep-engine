#pragma once

#include <glm\vec2.hpp>
#include <glm\vec4.hpp>

#include <vector>

#include <PxPhysicsAPI.h>

namespace glen
{
	class Shader;
	class Texture;
	class Physics;
}

class BaseCamera;

// This class handles the generation of terrain. It's currently very tightly coupled to the PerlinNoise class.
// Ultimately it should be able to use different types of noise generation. It's also not particularly memory efficient.
class Terrain
{
public:
	Terrain();
	~Terrain();

	// A vertex object to hold the information we need to create and render the Terrain.
	struct Vertex
	{
		glm::vec4 position;
		glm::vec2 uv;
		glm::vec2 texuv;
	};

	bool GenerateTerrain(unsigned int rowCount, unsigned int columnCount, float scale = 1.0f, unsigned int xOrigin = 0, unsigned int yOrigin = 0);
	bool GenerateShaders();
	bool GeneratePerlinNoise(int octaves = 6, float amplitude = 1.0f, float persistence = 0.3f, float frequency = 1.0f, unsigned int seed = 0);
	void GenerateHeightMap(glen::Physics* physics);
	void Render(BaseCamera* camera);

private:
	// Some of this stuff could be better cached and organised
	unsigned int gridIndexCount;
	unsigned int gridVertexCount;
	unsigned int gridRows;
	unsigned int gridColumns;
	std::vector<float> perlinData;

	// Ultimately, this VAO/VBO/IBO stuff will be moved into its own class. For now, it resides here.
	unsigned int vao;
	unsigned int vbo;
	unsigned int ibo;

	unsigned int perlinTexture;
	physx::PxRigidStatic* heightMap;

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<glen::Texture*> textures;
	glen::Shader* shader;
};