#include "MessageBus.h"

#include "TestSystem.h"

// The following code is very, very WIP.

TestSystem1::TestSystem1()
{
	msgBus->RegisterSystem(this);
}

TestSystem1::~TestSystem1()
{
	msgBus->UnregisterSystem(this);
}

void TestSystem1::HandleMessage(glen::Message* msg)
{
	switch (msg->type)
	{
	case MSG_TYPE_DEFAULT:
		break;
	case MSG_TYPE_INPUT:
	{
		if (msg->text == "KEY_R")
		{
			// for example, change rendering mode.
			msgBus->PostMessage(glen::Message("RENDER_MODE_CHANGED", MSG_TYPE_RENDER));
		}
	}
	default:
		break;
	}
}




TestSystem2::TestSystem2()
{
	msgBus->RegisterSystem(this);
}

TestSystem2::~TestSystem2()
{
	msgBus->UnregisterSystem(this);
}

void TestSystem2::HandleMessage(glen::Message* msg)
{
	switch (msg->type)
	{
	case MSG_TYPE_DEFAULT:
		break;
	case MSG_TYPE_RENDER:
		break;
	default:
		break;
	}
}