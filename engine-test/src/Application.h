#pragma once

#include <glm\vec3.hpp>

#include <vector>

#include "Engine.h"

class Interface;
class FlyCamera;
class Terrain;
namespace glen
{
	class Physics;
	class FBXModel;
}

class Application : public Engine
{
	friend class Interface;
public:
	// ## CONSTRUCTORS AND DESTRUCTORS ########################################
	Application();

protected:
	// ## PROTECTED METHODS ###################################################
	// These methods should only be accessed by the Engine, and never called
	// anywhere else during the program.

	bool Startup() override;
	bool Update(float deltaTime) override;
	void Render() override;
	void Shutdown() override;
	
	// Interface functions
	// These are here to replace an attempt at moving this kind of thing into its own class
	// This is a better solution, for now.
	bool Interface_Startup();
	bool Interface_Update();
	void Interface_Render();
	void Interface_Shutdown();
	void Interface_Resize();
	void Interface_ShowMainMenuBar();

private:
	// ## PRIVATE MEMBERS #####################################################
	FlyCamera* camera;
	Terrain* terrain;
	glen::Physics* physics;
	glen::FBXModel* fbx;

	// These could also be temporary and created every frame, like the other
	// interface variables in the Interface_Update() function
	glm::vec3 clearColor;
	float cameraSpeed;

	// Terrain
	int terrainGridSize;
	int terrainOctaves;
	float terrainAmplitude;
	float terrainPersistence;
	float terrainFrequency;
	int terrainSeed;
};