#pragma once

#include "Gui.h"

class Application;
class Window;

// This is a failed attempt at moving ImGui related functionality
// into its own class. This isn't how you do encapsulation, kids!
class Interface : public Gui
{
public:
	Interface() = delete;
	Interface(Window* window);

	// ew no, ew no, no, don't do that, ew
	bool Update(Application* app);
};