#version 410
in vec2 vTexCoord;
in float vHeight;
out vec4 FragColor;
uniform sampler2D PerlinTexture;
vec4 deepWaterColor = vec4(0, 0, 1, 1);
vec4 waterColor = vec4(0.1, 0.4, 0.7, 1);
vec4 sandColor = vec4(1.0, 1.0, 0.6, 1);
vec4 plainsColor = vec4(0.6, 0.75, 0.25, 1);
vec4 forestColor = vec4(0.3, 0.5, 0.2, 1);
vec4 deepForestColor = vec4(0, 0.4, 0.2, 1);
vec4 lowMountainsColor = vec4(0.6, 0.6, 0.6, 1);
vec4 highMountainsColor = vec4(0.8, 0.8, 0.8, 1);
vec4 snowColor = vec4(1, 1, 1, 1);

void main()
{
	float blend = smoothstep(0.0, 0.2, vHeight);
	vec4 result = mix(deepWaterColor, waterColor, blend);
	blend = smoothstep(0.2, 0.4, vHeight);
	vec4 result2 = mix(result, sandColor, blend);
	blend = smoothstep(0.4, 1.2, vHeight);
	vec4 result3 = mix(result2, plainsColor, blend);
	blend = smoothstep(1.2, 4.0, vHeight);
	vec4 result4 = mix(result3, forestColor, blend);
	blend = smoothstep(4.0, 8.0, vHeight);
	vec4 result5 = mix(result4, deepForestColor, blend);
	blend = smoothstep(8.0, 16.0, vHeight);
	vec4 result6 = mix(result5, lowMountainsColor, blend);
	blend = smoothstep(16.0, 28.0, vHeight);
	vec4 result7 = mix(result6, highMountainsColor, blend);
	blend = smoothstep(28.0, 40.0, vHeight);
	vec4 result8 = mix(result7, snowColor, blend);
	FragColor = result8;
}