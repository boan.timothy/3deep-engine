#version 410
in vec2 vTexCoord;
in float vHeight;
out vec4 FragColor;
uniform sampler2D Texture0;
uniform sampler2D Texture1;
uniform sampler2D Texture2;
uniform sampler2D Texture3;
uniform sampler2D Texture4;
vec4 Texture0Color = texture(Texture0, vTexCoord);
vec4 Texture1Color = texture(Texture1, vTexCoord);
vec4 Texture2Color = texture(Texture2, vTexCoord);
vec4 Texture3Color = texture(Texture3, vTexCoord);
vec4 Texture4Color = texture(Texture4, vTexCoord);

void main()
{
	float blend = smoothstep(0.15, 0.25, vHeight); // blend zone between Texture0 and Texture1
	vec4 result = mix(Texture0Color, Texture1Color, blend);
	blend = smoothstep(0.25, 0.4, vHeight); // blend zone between Texture1 and Texture2
	result = mix(result, Texture2Color, blend);
	blend = smoothstep(0.9, 1.5, vHeight); // blend zone between Texture2 and Texture3
	result = mix(result, Texture3Color, blend);
	blend = smoothstep(1.6, 1.9, vHeight); // blend zone between Texture3 and Texture4
	result = mix(result, Texture4Color, blend);
	
	FragColor = result;
}