#version 410
layout(location=0) in vec4 Position;
layout(location=1) in vec2 TexCoord;
out vec2 vTexCoord;
out float vHeight;
uniform mat4 ProjectionView;
uniform sampler2D PerlinTexture;

void main()
{
	vec4 pos = Position;
	pos.y += texture(PerlinTexture, TexCoord).r * 25;
	vTexCoord = TexCoord;
	vHeight = pos.y;
	gl_Position = ProjectionView * pos;
}