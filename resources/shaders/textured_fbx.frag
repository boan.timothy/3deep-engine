#version 410

in vec4 vPosition;
in vec4 vColor;
in vec4 vNormal;
in vec4 vTangent;
in vec4 vBiNormal;
in vec4 vIndices;
in vec4 vWeights;
in vec2 vTexCoord1;
in vec2 vTexCoord2;

out vec4 FragColor;

uniform sampler2D diffuse;
uniform vec3 cameraPosition;
uniform vec3 lightDirection;
uniform vec4 lightColor;
uniform float specularPower;

void main()
{
	float d = max(0, dot(normalize(vNormal.xyz), normalize(lightDirection)));
	vec3 vectorToCamera = normalize(cameraPosition - vPosition.xyz);
	vec3 reflection = reflect(normalize(-lightDirection), vNormal.xyz);
	float specular = max(0, dot(vectorToCamera, reflection));
	specular = pow(specular, specularPower);

	FragColor = texture(diffuse, vTexCoord1) * vec4(lightColor.rgb * d + lightColor.rgb * specular, lightColor.a);
}