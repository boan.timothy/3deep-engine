#version 410

layout(location=0) in vec4 Position;
layout(location=1) in vec4 Color;
layout(location=2) in vec4 Normal;
layout(location=3) in vec4 Tangent;
layout(location=4) in vec4 BiNormal;
layout(location=5) in vec4 Indices;
layout(location=6) in vec4 Weights;
layout(location=7) in vec2 TexCoord1;
layout(location=8) in vec2 TexCoord2;

out vec4 vPosition;
out vec4 vColor;
out vec4 vNormal;
out vec4 vTangent;
out vec4 vBiNormal;
out vec4 vIndices;
out vec4 vWeights;
out vec2 vTexCoord1;
out vec2 vTexCoord2;

uniform mat4 projectionView;

void main()
{
	vPosition = Position;
	vColor = Color;
	vNormal = Normal;
	vTangent = Tangent;
	vBiNormal = BiNormal;
	vIndices = Indices;
	vWeights = Weights;
	vTexCoord1 = TexCoord1;
	vTexCoord2 = TexCoord2;

	gl_Position = projectionView * Position;
}