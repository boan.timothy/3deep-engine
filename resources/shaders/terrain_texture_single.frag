#version 410
in vec2 vTexCoord;
out vec4 FragColor;
uniform sampler2D TextureDiffuse;

void main()
{
	FragColor = texture(TextureDiffuse, vTexCoord);
}