#version 410
layout(location=0) in vec4 Position;
layout(location=1) in vec2 TexCoord;
layout(location=2) in vec2 TexCoord2;
out vec2 vTexCoord;
out float vHeight;
uniform sampler2D PerlinTexture;
uniform mat4 ProjectionView;

void main()
{
	vec4 pos = Position;
	vHeight = texture(PerlinTexture, TexCoord).r;
	pos.y += vHeight * 25;
	vTexCoord = TexCoord2;
	gl_Position = ProjectionView * pos;
}