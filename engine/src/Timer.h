#pragma once

// The Timer handles time updates, it's as simple as it sounds really.
// This info can be called on for the Engine's update loop.
class Timer
{
public:
	Timer();

	void Update();

	float GetDeltaTime();

private:
	float deltaTime;
	float currentTime;
	float previousTime;
};