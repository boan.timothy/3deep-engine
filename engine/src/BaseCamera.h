#pragma once

#include <glm\matrix.hpp>

// The base class from which every other camera type should derive.
// This class makes use of better const correctness than other areas of the project!
// Try and carry this forward!
class BaseCamera
{
public:
	BaseCamera();
	BaseCamera(glm::vec3 position);
	BaseCamera(glm::mat4 transform);
	virtual ~BaseCamera() {};

	virtual void Update(double deltaTime) = 0;

	void SetWorldTransform(glm::mat4 transform);
	const glm::mat4 GetWorldTransform() const; // const after function = won't modify members unless mutable

	void SetPosition(glm::vec3 position);
	glm::vec3 GetPosition() const { return glm::vec3(worldTransform[3].x, worldTransform[3].y, worldTransform[3].z); }

	void SetLookAt(const glm::vec3& lookAt, const glm::vec3& up);
	void SetLookAt(const glm::vec3& position, const glm::vec3& lookAt, const glm::vec3& up);

	void SetPerspective(float fieldOfView, float aspectRatio, float near = 0.1f, float far = 1000.0f);

	const glm::mat4& GetProjection() const { return projectionTransform; }
	//TODO: maybe fix this inverse, maybe not, as it might increase frame times
	const glm::mat4& GetView() const { return glm::inverse(worldTransform); }
	const glm::mat4& GetProjectionView() const { return projectionViewTransform; }

	bool GetPerspectiveSet() const { return isPerspectiveSet; };

protected:
	void UpdateProjectionViewTransform();

	glm::mat4 projectionTransform;
	glm::mat4 projectionViewTransform;

private:
	glm::mat4 worldTransform;
	bool isPerspectiveSet;
};