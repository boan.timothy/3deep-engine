#define SUPPRESS_GL_CALLBACKS_NONE

#include "Engine.h"

#include "Frameworks\Graphics.h"
#include <Gizmos.h>

#include <iostream>

#include "Window.h"
#include "Timer.h"

Engine::Engine()
{
	isEngineRunning = false;
}

Engine::~Engine()
{
	if (window != nullptr)
		delete window;

	if (timer != nullptr)
		delete timer;
}

void Engine::Run()
{
	if (!isEngineRunning)
	{
		Initialise();

		Startup();

		isEngineRunning = true;

		while (!glfwWindowShouldClose(window->GetWindow()) && glfwGetKey(window->GetWindow(), GLFW_KEY_ESCAPE) != GLFW_PRESS && isEngineRunning)
		{
			timer->Update();

			isEngineRunning = Update(timer->GetDeltaTime());

			glfwPollEvents();

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			Render();

			Gizmos::clear();

			glfwSwapBuffers(window->GetWindow());
		}

		Shutdown();

		window->DestroyWindow();

		glfwTerminate();
	}
	else
	{
		// This is here because passing the pointer to an inherited Engine class into an Interface class was a thing.
		// If that sounds horrible, that's because it is, and that's why it doesn't happen anymore. This in theory should never
		// ever be called twice, but doing so doesn't result in anything bad happening.
		std::cout << "WARNING: Attempted to access Engine::Run() while Engine is already running!" << std::endl;
	}
}

bool Engine::Initialise()
{
	if (glfwInit() == false)
	{
		std::cout << "Unable to initialise OpenGL." << std::endl;
		return false;
	}

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	window = new Window;
	window->InitialiseWindow("Virtual World", Window::Windowed, 1280, 720);

	glfwMakeContextCurrent(window->GetWindow());

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		std::cout << "Failed to load OpenGL functions. Check your graphics drivers." << std::endl;
		
		window->DestroyWindow();
		glfwTerminate();
		return false;
	}

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_FRONT_AND_BACK);

	timer = new Timer();

	//TurnOnDebugLogging();
	glen::Graphics::EnableDebugLogging();

	return true;
}