#include <gl_core_4_4.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include "Shader.h"

using namespace glen;

Shader::Shader(std::string pathToVertexShader, std::string pathToFragmentShader)
{
	// Firstly, we want to retrieve the relevant shader code from the files
	std::string vertexShader;
	std::string fragmentShader;
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;

	// Allow our ifstream objects to throw exceptions
	vShaderFile.exceptions(std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::badbit);

	try
	{
		// Open our shader files
		vShaderFile.open(GetResourcePath() + pathToVertexShader);
		fShaderFile.open(GetResourcePath() + pathToFragmentShader);
		std::stringstream vShaderStream, fShaderStream;

		// Read the file buffers into the stringstreams
		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();

		// Close our file handlers
		vShaderFile.close();
		fShaderFile.close();

		// Store contents of stringstreams into our strings
		vertexShader = vShaderStream.str();
		fragmentShader = fShaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		printf("ERROR: [SHADER] FILE NOT SUCCESSFULLY READ\n");
		return;
	}

	const char* vShaderCode = vertexShader.c_str();
	const char* fShaderCode = fragmentShader.c_str();

	// Now, we want to compile our shaders

	CompileShader(vShaderCode, GL_VERTEX_SHADER);
	CompileShader(fShaderCode, GL_FRAGMENT_SHADER);

	LinkShaders();
}

Shader::~Shader()
{
	glDeleteProgram(shaderProgram);
}

bool Shader::LoadVertexShader(std::string pathToVertexShader)
{
	std::string fullPath = GetResourcePath() + pathToVertexShader;
	printf(fullPath.c_str());
	printf("\n");
	// Firstly, we want to retrieve the relevant shader code from the file
	std::string vertexShader;
	std::ifstream vShaderFile;

	// Allow our ifstream object to throw exceptions
	vShaderFile.exceptions(std::ifstream::badbit);

	try
	{
		// Open our shader file
		vShaderFile.open(fullPath);
		std::stringstream vShaderStream;

		// Read the file buffer into the stringstream
		vShaderStream << vShaderFile.rdbuf();

		// Close our file handler
		vShaderFile.close();

		// Store contents of stringstream into our string
		vertexShader = vShaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		printf("ERROR: [SHADER] VERTEX SHADER FILE NOT SUCCESSFULLY READ\n");
		return false;
	}

	const char* vShaderCode = vertexShader.c_str();

	// Now, we want to compile our shader
	return CompileShader(vShaderCode, GL_VERTEX_SHADER);
}

bool Shader::LoadFragmentShader(std::string pathToFragmentShader)
{
	std::string fullPath = GetResourcePath() + pathToFragmentShader;
	printf(fullPath.c_str());
	printf("\n");
	// Firstly, we want to retrieve the relevant shader code from the file
	std::string fragmentShader;
	std::ifstream fShaderFile;

	// Allow our ifstream object to throw exceptions
	fShaderFile.exceptions(std::ifstream::badbit);

	try
	{
		// Open our shader file
		fShaderFile.open(fullPath);
		std::stringstream fShaderStream;

		// Read the file buffer into the stringstream
		fShaderStream << fShaderFile.rdbuf();

		// Close our file handler
		fShaderFile.close();

		// Store contents of stringstream into our string
		fragmentShader = fShaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		printf("ERROR: [SHADER] FRAGMENT SHADER FILE NOT SUCCESSFULLY READ\n");
		return false;
	}

	const char* fShaderCode = fragmentShader.c_str();

	// Now, we want to compile our shader
	return CompileShader(fShaderCode, GL_FRAGMENT_SHADER);
}

//bool Shader::LoadGeometryShader(std::string pathToGeometryShader)
//{
//	return false;
//}

//bool Shader::LoadTessControlShader(std::string pathToTessControlShader)
//{
//	return false;
//}

//bool Shader::LoadTessEvaluationShader(std::string pathToTessEvaluationShader)
//{
//	return false;
//}

//bool Shader::LoadComputeShader(std::string pathToComputeShader)
//{
//	return false;
//}

bool Shader::CompileShader(const char* shader, GLenum type)
{
	unsigned int handle;
	int success;
	char infoLog[512];

	// Compile shader
	handle = glCreateShader(type);
	glShaderSource(handle, 1, &shader, 0);
	glCompileShader(handle);
	// Print any compile errors
	glGetShaderiv(handle, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(handle, 512, 0, infoLog);
		printf("ERROR: [SHADER] SHADER COMPILATION FAILED\n");
		printf("%s", infoLog);
		return false;
	}

	shadersToLink.emplace_back(handle);
	return true;
}

bool Shader::LinkShaders()
{
	int success;
	char infoLog[512];

	shaderProgram = glCreateProgram();

	// Attach all of the shaders ready to be linked
	for (auto& shader : shadersToLink)
	{
		glAttachShader(shaderProgram, shader);
	}

	// Link the shader program
	glLinkProgram(shaderProgram);
	// Print any linking errors
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, 0, infoLog);
		printf("ERROR: [SHADER] PROGRAM LINKING FAILED\n");
		printf("%s", infoLog);
		return false;
	}

	// Delete the shaders as they're linked into our program now and no longer necessary
	for (auto& shader : shadersToLink)
	{
		glDeleteShader(shader);
	}
	shadersToLink.resize(0);
	return true;
}

// Checks if the application was launched inside a debugger, and returns the path to 
// the resource folder. Due to the way Visual Studio handles working directories, the
// resources folder is only three directories above the working directory, while running
// the application from the exe means the resources folder actually is four directories up.
std::string Shader::GetResourcePath()
{
	if (IsDebuggerPresent())
		return "../../../resources/";

	return "../../../../resources/";
}