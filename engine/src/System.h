#pragma once

namespace glen
{
	struct Message;
	class MessageBus;

	// This class ties into the MessageBus system, but again, didn't make the cut
	// for this project in time. Ultimately, other Systems would inherit from this.

	// A System is any class that adds functionality to the engine. Systems
	// should only communicate with the Message Bus (allowing communication
	// with other Systems) and the Frameworks (allowing communication with low
	// level APIs.
	class System
	{
	public:
		System();
		~System();

		virtual void HandleMessage(Message* msg) = 0;

	protected:
		MessageBus* msgBus;
	};
}