#include "FlyCamera.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

#include <glm\ext.hpp>


FlyCamera::FlyCamera() : FlyCamera(10.0f, 0.3f)
{

}

FlyCamera::FlyCamera(float flySpeed, float rotSpeed) : BaseCamera()
{
	this->flySpeed = flySpeed;
	this->rotationSpeed = rotSpeed;
	window = nullptr;
}

FlyCamera::~FlyCamera()
{

}

void FlyCamera::Update(double deltaTime)
{
	assert(window != nullptr);

	HandleKeyboardInput(deltaTime);
	HandleMouseInput(deltaTime);
}

void FlyCamera::SetFlySpeed(float fSpeed)
{
	flySpeed = fSpeed;
}

float FlyCamera::GetFlySpeed() const
{
	return flySpeed;
}

void FlyCamera::SetRotationSpeed(float val)
{
	rotationSpeed = val;
}

float FlyCamera::GetRotationSpeed() const
{
	return rotationSpeed;
}

void FlyCamera::SetInputWindow(GLFWwindow* pWindow)
{
	window = pWindow;
}

void FlyCamera::HandleKeyboardInput(double deltaTime)
{
	// Get the cameras vectors
	glm::mat4 trans = GetWorldTransform();

	glm::vec3 vRight = glm::vec3(trans[0].x, trans[0].y, trans[0].z);
	glm::vec3 vUp = glm::vec3(trans[1].x, trans[1].y, trans[1].z);
	glm::vec3 vForward = glm::vec3(trans[2].x, trans[2].y, trans[2].z);
	glm::vec3 moveDir(0.0f);

	if (glfwGetKey(window, GLFW_KEY_W))
	{
		moveDir -= vForward;
	}
	if (glfwGetKey(window, GLFW_KEY_S))
	{
		moveDir += vForward;
	}
	if (glfwGetKey(window, GLFW_KEY_A))
	{
		moveDir -= vRight;
	}
	if (glfwGetKey(window, GLFW_KEY_D))
	{
		moveDir += vRight;
	}
	if (glfwGetKey(window, GLFW_KEY_R))
	{
		moveDir += vUp;
	}
	if (glfwGetKey(window, GLFW_KEY_F))
	{
		moveDir -= vUp;
	}

	float fLength = glm::length(moveDir);
	if (fLength > 0.01f)
	{
		moveDir = ((float)deltaTime * flySpeed) * glm::normalize(moveDir);
		SetPosition(GetPosition() + moveDir);
	}
}

void FlyCamera::HandleMouseInput(double deltaTime)
{
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
	{
		if (viewButtonClicked == false)
		{
			int width, height;
			glfwGetFramebufferSize(window, &width, &height);

			cursorXPos = width / 2.0;
			cursorYPos = height / 2.0;

			glfwSetCursorPos(window, width / 2, height / 2);

			viewButtonClicked = true;
		}
		else
		{
			double mouseX, mouseY;
			glfwGetCursorPos(window, &mouseX, &mouseY);
			double xOffset = mouseX - cursorXPos;
			double yOffset = mouseY - cursorYPos;

			CalculateRotation(deltaTime, xOffset, yOffset);
		}

		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		glfwSetCursorPos(window, width / 2, height / 2);
	}
	else
	{
		viewButtonClicked = false;
	}
}

void FlyCamera::CalculateRotation(double deltaTime, double xOffset, double yOffset)
{
	if (yOffset != 0.0f)
	{
		glm::mat4 rot = glm::rotate((float)(rotationSpeed * deltaTime * -yOffset), glm::vec3(1, 0, 0));
		SetWorldTransform(GetWorldTransform() * rot);
	}

	if (xOffset == 0 && yOffset == 0) return;

	if (xOffset != 0.0f)
	{
		glm::mat4 rot = glm::rotate((float)(rotationSpeed * deltaTime * -xOffset), glm::vec3(0, 1, 0));
		SetWorldTransform(GetWorldTransform() * rot);
	}

	// clean up
	glm::mat4 oldTrans = GetWorldTransform();
	glm::mat4 trans;
	glm::vec3 up = glm::vec3(0, 1, 0);

	// Right
	glm::vec3 oldForward = glm::vec3(oldTrans[2].x, oldTrans[2].y, oldTrans[2].z);
	trans[0] = glm::normalize(glm::vec4(glm::cross(up, oldForward), 0));

	// Up
	glm::vec3 newRight = glm::vec3(trans[0].x, trans[0].y, trans[0].z);
	trans[1] = glm::normalize(glm::vec4(glm::cross(oldForward, newRight), 0));

	//Forward
	trans[2] = glm::normalize(oldTrans[2]);

	//Position
	trans[3] = oldTrans[3];

	SetWorldTransform(trans);
}