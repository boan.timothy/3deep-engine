#pragma once

#include "glm\vec2.hpp"

struct GLFWwindow;

// This class is responsible for handling the instantiation and destruction of a GLFWwindow.
// In theory, it could be expanded upon to support other window management solutions.
class Window
{
public:
	enum VideoMode
	{
		Windowed = 0,
		Fullscreen
	};

	Window();
	~Window();

	//static
	Window* InitialiseWindow(const char* title, VideoMode vm, unsigned int width = 0, unsigned int height = 0);
	void DestroyWindow();

	GLFWwindow* GetWindow();
	glm::ivec2 GetDisplaySize();

private:

	GLFWwindow* window;
};