#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

#include "Frameworks\FileIO.h"
#include "Texture.h"

using namespace glen;

Texture::Texture(std::string pathToTextureImage, bool completePath)
{
	texture = 0;
	imageWidth = 0;
	imageHeight = 0;
	imageFormat = 0;
	LoadTexture(pathToTextureImage, completePath);
}

Texture::~Texture()
{

}

bool Texture::LoadTexture(std::string pathToTextureImage, bool completePath)
{
		if (!completePath)
			pathToTextureImage = FileIO::GetResourcePath() + pathToTextureImage;

		printf(pathToTextureImage.c_str());
		printf("\n");
		unsigned char* data = stbi_load(pathToTextureImage.c_str(), &imageWidth, &imageHeight, &imageFormat, STBI_default);
	
	if (data != nullptr)
	{
		// Generate texture handle
		glGenTextures(1, &texture);
		// Bind texture to 2D texture slot
		glBindTexture(GL_TEXTURE_2D, texture);
		// Specify the texture data
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		// Generate texture mipmaps
		glGenerateMipmap(GL_TEXTURE_2D);
		// Specify texture filtering mode
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		// Specify the texture wrapping mode
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		stbi_image_free(data);

		return true;
	}

	return false;
}
