#pragma once

#include "BaseCamera.h"

struct GLFWwindow;

// A flying camera that can be controlled using standard keyboard controls
// Controls are mapped to be relative to the camera, so facing down and then
// pressing "R" will result in the camera moving forwards in world space.
// This perhaps isn't the most immediately intuitive result, but it works.

// #### CONTROLS ####
// W: Forwards
// S: Backwards
// A: Strafe Left
// D: Strafe Right
// R: Fly Up
// F: Fly Down
// Right mouse: Rotate camera

class FlyCamera : public BaseCamera
{
public:
	FlyCamera();
	FlyCamera(float flySpeed, float rotSpeed);
	~FlyCamera();

	virtual void Update(double deltaTime);

	void SetFlySpeed(float fSpeed);
	float GetFlySpeed() const;
	void SetRotationSpeed(float val);
	float GetRotationSpeed() const;

	// TODO: Setup a proper Input Manager
	void SetInputWindow(GLFWwindow* pWindow);

protected:
	void HandleKeyboardInput(double deltaTime);
	void HandleMouseInput(double deltaTime);

	void CalculateRotation(double deltaTime, double xOffset, double yOffset);

	GLFWwindow* window;
	float flySpeed;
	float rotationSpeed;
	bool viewButtonClicked;
	double cursorXPos, cursorYPos;
};