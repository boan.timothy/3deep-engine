#pragma once
#include <string>
#include <vector>

namespace glen
{
	// This Shader class enables the loading of GLSL shaders into a program.
	// There is capacity here to support other parts of the graphics pipline,
	// such as tesselation or compute stages, but for now the only shader support
	// enabled is for vertex and fragment shading.
	class Shader
	{
	public:
		Shader() {};
		Shader(std::string pathToVertexShader, std::string pathToFragmentShader);
		~Shader();

		bool LoadVertexShader(std::string pathToVertexShader);
		bool LoadFragmentShader(std::string pathToFragmentShader);
		//bool LoadGeometryShader(std::string pathToGeometryShader);
		//bool LoadTessControlShader(std::string pathToTessControlShader);
		//bool LoadTessEvaluationShader(std::string pathToTessEvaluationShader);
		//bool LoadComputeShader(std::string pathToComputeShader);
		bool LinkShaders();

		unsigned int GetShaderProgram() { return shaderProgram; }

	private:
		bool CompileShader(const char* shader, GLenum type);
		std::string GetResourcePath();

		unsigned int shaderProgram;
		std::vector<unsigned int> shadersToLink;
	};
}