#pragma once

#include <string>
#include <vector>

// This header stamps out the MessageBus system that will eventually be the sole method
// of inter-system communication. However, it is still in its very early stages and thus
// isn't actually used anywhere just yet. The end goal is to have this be the only way
// for one system to communicate with the others.


// Inspired by RakNet, this enum defines the different message types. Some example types are given,
// and an application using this system can define its own types for its own systems by creating a new
// enum and making the first value equal to MSG_TYPE_USER_ENUM. This enum is not representative of the
// finished product, and any enumerators here are subject to change.
enum DefaultMessageTypes
{
	MSG_TYPE_DEFAULT = 0,
	MSG_TYPE_RENDER,
	MSG_TYPE_INPUT,
	MSG_TYPE_SCENE,
	MSG_TYPE_AUDIO,

	// use this enumerator for application specific types
	MSG_TYPE_USER_ENUM
};


namespace glen
{
	// Message structure, used to communicate between systems.
	struct Message
	{
		Message();// = delete;
		Message(std::string text, DefaultMessageTypes type);
		
		std::string text;
		DefaultMessageTypes type;
	};

	class System;
	class MessageBus
	{
	public:
		MessageBus();
		~MessageBus();

		void Startup();
		void Update();
		void Shutdown();

		void PostMessage(Message msg);
		void RegisterSystem(System* sys);
		void UnregisterSystem(System* sys);

	private:
		std::vector<System*> systems;

		unsigned int head;
		unsigned int tail;
		static const unsigned int MAX_MESSAGES = 128;
		Message queue[MAX_MESSAGES];
	};
}