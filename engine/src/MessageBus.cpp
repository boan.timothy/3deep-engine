#include <cassert>
#include "System.h"

#include "MessageBus.h"

using namespace glen;

MessageBus::MessageBus()
{
	head = 0;
	tail = 0;
}

MessageBus::~MessageBus()
{

}

void MessageBus::Startup()
{

}

void MessageBus::Shutdown()
{

}

void MessageBus::Update()
{
	// If there are no pending messages, return
	if (head == tail)
		return;

	// Walk over all the messages in the queue and broadcast them to every system
	// This could be improved by broadcasting only to relevant systems.
	for (unsigned int i = head; i != tail; i = (i + 1) % MAX_MESSAGES)
	{
		for (System* sys : systems)
		{
			sys->HandleMessage(&queue[head]);
		}
	}

	head = (head + 1) % MAX_MESSAGES;
}

void MessageBus::PostMessage(Message msg)
{
	assert((tail + 1) % MAX_MESSAGES != head);

	// Add message to the end of the queue
	queue[tail].text = msg.text;
	queue[tail].type = msg.type;
	tail++;
}

void MessageBus::RegisterSystem(System* sys)
{
	for (System* s : systems)
	{
		assert(sys == s);
	}

	systems.emplace_back(sys);
}

void MessageBus::UnregisterSystem(System* sys)
{
	for (auto iter = systems.begin(); iter != systems.end(); iter++)
	{
		if (sys == *iter)
		{
			systems.erase(iter);
		}
	}
}

Message::Message()
{

}

Message::Message(std::string text, DefaultMessageTypes type)
{
	this->text = text;
	this->type = type;
}
