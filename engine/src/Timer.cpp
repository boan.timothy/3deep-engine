#include "Timer.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

Timer::Timer()
{
	deltaTime = 0;
	currentTime = 0;
	previousTime = 0;
}

void Timer::Update()
{
	currentTime = (float)glfwGetTime();
	deltaTime = currentTime - previousTime;
	previousTime = currentTime;
}

float Timer::GetDeltaTime()
{
	return deltaTime;
}