#pragma once

//#define STB_IMAGE_IMPLEMENTATION

#include <string>

#include <stb_image.h>
#include <glm\vec2.hpp>

namespace glen
{
	// This class enables the loading of image files to be stored as OpenGL textures.
	// It's not the most robust class, but it does the job needed for this project.
	class Texture
	{
	public:
		Texture() = delete;
		Texture(std::string pathToTextureImage, bool completePath = false);
		~Texture();

		bool LoadTexture(std::string pathToTextureImage, bool completePath);
		
		unsigned int GetTexture() { return texture; }
		int	         GetWidth()   { return imageWidth; }
		int          GetHeight()  { return imageHeight; }

	private:
		unsigned int texture;
		int imageWidth, imageHeight, imageFormat;
	};
}