#pragma once

struct GLFWwindow;
class Window;
class Timer;

class Engine
{
public:
	// ## PUBLIC METHODS ######################################################
	// The only function of the engine that external sources (such as a project
	// main file) should access is the Run() function.
	void Run();

protected:
	// ## CONSTRUCTORS AND DESTRUCTORS ########################################
	// The constructor and destructor are protected, so they may not be called
	// except by an inherited class.
	Engine();
	~Engine();

	// ## PROTECTED METHODS ###################################################	

	// Pure virtual engine functions that should be overwritten on an
	// application-by-application basis.
	virtual bool Startup() = 0;
	virtual bool Update(float deltaTime) = 0;
	virtual void Render() = 0;
	virtual void Shutdown() = 0;

	// ## PROTECTED MEMBERS ###################################################
	Window* window;
	Timer* timer;

private:
	// ## PRIVATE METHODS #####################################################
	bool Initialise();

	// ## PRIVATE MEMBERS #####################################################
	bool isEngineRunning;
};