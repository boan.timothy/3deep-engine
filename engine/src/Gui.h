#pragma once

#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"

class Window;

// A class to encapsulate the various functions for ImGui and the helper
// This was originally designed for a different interface system, so it
// functions fine but might need to be looked at with the new system in mind.
class Gui
{
public:
	Gui() = delete;
	Gui(Window* window);
	~Gui();

	void NewFrame();
	void Render();
	
private:
	bool Initialise(Window* window);
	void Shutdown();
};