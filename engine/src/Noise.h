#pragma once

// This header defines different noise algorithms, if you count one noise algorithm as "different".
// In future, other noise generation classes will reside here.

namespace glen
{
	// Custom basic perlin noise generator, heavily based on code I scavenged from Stack Overflow somewhere.
	class PerlinNoise
	{
	public:
		PerlinNoise();
		PerlinNoise(float amplitude, float frequency, float persistence, unsigned int octaves, unsigned int seed);

		float GetHeight(float x, float y) const;

		float Amplitude()   const { return amplitude; }
		float Frequency()   const { return frequency; }
		float Persistence() const { return persistence; }
		int   Octaves()     const { return octaves; }

		void Set(float amplitude, float frequency, float persistence, unsigned int octaves, unsigned int seed);

		void SetAmplitude  (float amplitude)   { this->amplitude = amplitude;     }
		void SetFrequency  (float frequency)   { this->frequency = frequency;     }
		void SetPersistence(float persistence) { this->persistence = persistence; }
		void SetOctaves    (int    octaves)    { this->octaves = octaves;         }
		void SetSeed       (unsigned int seed) { this->seed = seed; }

	private:

		float Total(float i, float j) const;
		float GetValue(float x, float y) const;
		float Interpolate(float x, float y, float a) const;
		float Noise(int x, int y) const;

		float amplitude;
		float frequency;
		float persistence;
		int octaves;
		unsigned int seed;
	};
}