#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <Gizmos.h>
//#include <ParticleEmitter.h>
#include <ParticleFluidEmitter.h>
#include "Physics.h"

using namespace glen;

void Physics::Startup()
{
	// Create a callback function pointer to our custom memory manager
	//PxAllocatorCallback* callback = new PhysicsAllocator();
	// Or use the default PhysX callback
	PxAllocatorCallback* callback = (PxAllocatorCallback*)&defaultAllocatorCallback;

	// Instantiate a PxFoundation singleton and store a pointer to it
	foundation = PxCreateFoundation(PX_PHYSICS_VERSION, *callback, defaultErrorCallback);

	// Instantiate a physics system and store a pointer to it
	physics = PxCreatePhysics(PX_PHYSICS_VERSION, *foundation, PxTolerancesScale());

	// Initialise the extension library (similar to OpenGL)
	PxInitExtensions(*physics);

	// Create physics material
	material = physics->createMaterial(0.5f, 0.5f, 0.5f);

	// Create a scene description object, which controls the parameters of the scene
	PxSceneDesc sceneDesc(physics->getTolerancesScale());
	sceneDesc.gravity = PxVec3(0, -10.0f, 0);
	sceneDesc.filterShader = &PxDefaultSimulationFilterShader; // catches triggered events
	cpuDispatcher = PxDefaultCpuDispatcherCreate(1); // use a single thread for PhysX
	sceneDesc.cpuDispatcher = cpuDispatcher;
	scene = physics->createScene(sceneDesc);
}

void Physics::Shutdown()
{
	// Very thorough checking and deleting - PhysX was resulting in a lot of memory leaks so
	// extra care was taken here to release and delete everything that's active before
	// deleting the foundation (which won't delete if anything else is still using it)
	if (particleEmitter)
	{
		delete particleEmitter;
		particleEmitter = nullptr;
	}
	if (particleSystem)
	{
		particleSystem->release();
		particleSystem = nullptr;
	}
	if (playerController)
	{
		playerController->release();
		playerController = nullptr;
	}
	if (controllerManager)
	{
		controllerManager->release();
		controllerManager = nullptr;
	}
	if (hitReport)
	{
		delete hitReport;
		hitReport = nullptr;
	}
	if (connection)
	{
		connection->disconnect();
		connection->release();
		connection = nullptr;
	}
	if (scene)
	{
		scene->release();
		scene = nullptr;
	}
	if (cpuDispatcher)
	{
		cpuDispatcher->release();
		cpuDispatcher = nullptr;
	}
	if (material)
	{
		material->release();
		material = nullptr;
	}
	if (physics)
	{
		physics->release();
		physics = nullptr;
	}
	PxCloseExtensions();
	if (foundation)
	{
		foundation->release();
		foundation = nullptr;
	}
}

void Physics::Update(float deltaTime, GLFWwindow* window)
{	
	if (deltaTime <= 0)
		return;

	if (deltaTime > 0)
	{
		UpdatePlayerController(deltaTime, window);
		UpdateFluidDynamics(deltaTime);
		
		scene->simulate(deltaTime > 0.33f ? 0.33f : deltaTime);
		while (scene->fetchResults() == false);
	}
	RenderGizmos(scene);
}

void Physics::UpdatePlayerController(float deltaTime, GLFWwindow* window)
{
	bool onGround;
	float movementSpeed = 10.0f;
	float rotationSpeed = 1.0f;

	if (hitReport->getPlayerContactNormal().y > 0.3f)
	{
		velocity = -0.1f;
		onGround = true;
	}
	else
	{
		velocity += gravity * deltaTime;
		onGround = false;
	}
	hitReport->clearPlayerContactNormal();
	const PxVec3 up(0, 1, 0);
	PxVec3 velocity(0, velocity, 0);
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		velocity.x -= movementSpeed * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		velocity.x += movementSpeed * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		velocity.z += movementSpeed * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		velocity.z -= movementSpeed * deltaTime;
	}
	// jumping code goes here
	float minDistance = 0.001f;
	PxControllerFilters filter;
	PxQuat rotation(rotation, PxVec3(0, 1, 0));
	playerController->move(rotation.rotate(velocity), minDistance, deltaTime, filter);
}

void Physics::UpdateFluidDynamics(float deltaTime)
{
	if (particleEmitter)
	{
		particleEmitter->update(deltaTime);
		particleEmitter->renderParticles();
	}
}

PxRigidStatic* Physics::CreateHeightMap(int rowCount, int columnCount, void* sampleData, int heightScale)
{
	PxHeightFieldDesc desc;
	desc.format = PxHeightFieldFormat::eS16_TM;
	desc.nbRows = rowCount;
	desc.nbColumns = columnCount;

	float* data = static_cast<float*>(sampleData);
	int* adjustedData = new int[rowCount * columnCount];
	for (int i = 0; i < rowCount * columnCount; i++)
	{
		adjustedData[i] = static_cast<int>((data[i] * heightScale) + 0.8f);
	}

	desc.samples.data = adjustedData;
	desc.samples.stride = sizeof(PxHeightFieldSample);
	desc.thickness = -1000.0f;

	PxHeightField* heightField = physics->createHeightField(desc);
	PxHeightFieldGeometry geometry(heightField, PxMeshGeometryFlags(), (PxReal)1, (PxReal)1, (PxReal)1);
	PxRigidStatic* heightMap = PxCreateStatic(*physics, PxTransform(PxVec3(0 - rowCount * 0.5f, 0, 0 - columnCount * 0.5f)), geometry, *material);
	data = nullptr;
	delete[] adjustedData;
	heightField->release();
	return heightMap;
}

void Physics::SetUpVisualDebugger()
{
	// Check if PvdConnection manager is available on this platform
	if (physics->getPvdConnectionManager() == NULL)
	{
		connection = nullptr;
		return;
	}

	// Setup connection parameters
	const char* pvdHostIP = "127.0.0.1";
	int port = 5425;
	unsigned int timeout = 100; // milliseconds
	PxVisualDebuggerConnectionFlags flags = PxVisualDebuggerExt::getAllConnectionFlags();
	
	connection = PxVisualDebuggerExt::createConnection(physics->getPvdConnectionManager(), pvdHostIP, port, timeout, flags);
}

void Physics::SetUpDynamicObjects()
{
	////Add a plane
	//PxTransform pose = PxTransform(PxVec3(0.0f, 0, 0.0f), PxQuat(PxHalfPi * 1.0f, PxVec3(0.0f, 0.0f, 1.0f)));
	//PxRigidStatic* plane = PxCreateStatic(*physics, pose, PxPlaneGeometry(), *material);
	//scene->addActor(*plane);

	float density = 2;

	// Adds a box
	PxBoxGeometry box(2, 2, 2);
	PxTransform transform = PxTransform(PxVec3(-5, 10, -5));
	PxRigidDynamic* dynamicActor = PxCreateDynamic(*physics, transform, box, *material, density);
	scene->addActor(*dynamicActor);

	// Adds a capsule
	PxCapsuleGeometry capsule = PxCapsuleGeometry(2, 5);
	transform = PxTransform(PxVec3(5, 10, 5));
	PxRigidDynamic* dynamicActor2 = PxCreateDynamic(*physics, transform, capsule, *material, density);
	scene->addActor(*dynamicActor2);

	// Adds two spheres
	PxSphereGeometry sphere = PxSphereGeometry(5);
	transform = PxTransform(PxVec3(5, 5, 5));
	PxRigidDynamic* dynamicActor3 = PxCreateDynamic(*physics, transform, sphere, *material, density);
	scene->addActor(*dynamicActor3);
	transform = PxTransform(PxVec3(-5, 5, -5));
	PxRigidDynamic* dynamicActor4 = PxCreateDynamic(*physics, transform, sphere, *material, density);
	scene->addActor(*dynamicActor4);

}

void Physics::SetUpPlayerController()
{
	hitReport = new ControllerHitReport();
	controllerManager = PxCreateControllerManager(*scene);
	PxCapsuleControllerDesc desc;
	desc.height = 10.0f;
	desc.radius = 3.0f;
	desc.position.set(0, 0, 0);
	desc.material = material;
	desc.reportCallback = hitReport;
	desc.density = 30;

	playerController = controllerManager->createController(desc);
	playerController->setPosition(PxExtendedVec3(0, 0, 0));

	velocity = 0.0f;
	rotation = 0.0f;
	gravity = -0.5f;
	hitReport->clearPlayerContactNormal();
}

void Physics::SetUpFluidDynamics()
{
	PxU32 maxParticles = 1000;
	bool perParticleRestOffset = false;
	particleSystem = physics->createParticleFluid(maxParticles, perParticleRestOffset);

	particleSystem->setRestParticleDistance(0.3f);
	particleSystem->setDynamicFriction(0.1f);
	particleSystem->setStaticFriction(0.1f);
	particleSystem->setDamping(0.1f);
	particleSystem->setParticleMass(0.1f);
	particleSystem->setRestitution(0);
	particleSystem->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, true);
	particleSystem->setStiffness(100);

	if (particleSystem)
	{
		scene->addActor(*particleSystem);
		particleEmitter = new ParticleFluidEmitter(maxParticles, PxVec3(-10, 50, -10), particleSystem, 0.2f);
		particleEmitter->setStartVelocityRange(-3.0f, -0.5f, -3.0f, 3.0f, 0.5f, 3.0f);
		//particleEmitter->setStartVelocityRange(0.001f, -250.0f, -0.001f, 0.001f, -250.0f, 0.001f);
	}
}

void AddWidget(PxShape* shape, PxRigidActor* actor, glm::vec4 geo_color)
{
	PxTransform full_transform = PxShapeExt::getGlobalPose(*shape, *actor);
	glm::vec3 actor_position(full_transform.p.x, full_transform.p.y, full_transform.p.z);
	glm::quat actor_rotation(full_transform.q.w,
		full_transform.q.x,
		full_transform.q.y,
		full_transform.q.z);
	glm::mat4 rot(actor_rotation);

	glm::mat4 rotate_matrix = glm::rotate(10.f, glm::vec3(7, 7, 7));

	PxGeometryType::Enum geo_type = shape->getGeometryType();

	switch (geo_type)
	{
	case (PxGeometryType::eBOX) :
	{
		PxBoxGeometry geo;
		shape->getBoxGeometry(geo);
		glm::vec3 extents(geo.halfExtents.x, geo.halfExtents.y, geo.halfExtents.z);
		Gizmos::addAABBFilled(actor_position, extents, geo_color, &rot);
	} break;
	case (PxGeometryType::eCAPSULE) :
	{
		PxCapsuleGeometry geo;
		shape->getCapsuleGeometry(geo);
		Gizmos::addCapsule(actor_position, geo.halfHeight * 2, geo.radius, 16, 16, geo_color, &rot);
	} break;
	case (PxGeometryType::eSPHERE) :
	{
		PxSphereGeometry geo;
		shape->getSphereGeometry(geo);
		Gizmos::addSphereFilled(actor_position, geo.radius, 16, 16, geo_color, &rot);
	} break;
	case (PxGeometryType::ePLANE) :
	{

	} break;
	}
}

void Physics::RenderGizmos(PxScene* physics_scene)
{
	PxActorTypeFlags desiredTypes = PxActorTypeFlag::eRIGID_STATIC | PxActorTypeFlag::eRIGID_DYNAMIC;
	PxU32 actor_count = physics_scene->getNbActors(desiredTypes);
	PxActor** actor_list = new PxActor*[actor_count];
	physics_scene->getActors(desiredTypes, actor_list, actor_count);

	glm::vec4 geo_color(1, 0, 0, 1);
	for (int actor_index = 0;
	actor_index < (int)actor_count;
		++actor_index)
	{
		PxActor* curr_actor = actor_list[actor_index];
		if (curr_actor->isRigidActor())
		{
			PxRigidActor* rigid_actor = (PxRigidActor*)curr_actor;
			PxU32 shape_count = rigid_actor->getNbShapes();
			PxShape** shapes = new PxShape*[shape_count];
			rigid_actor->getShapes(shapes, shape_count);

			for (int shape_index = 0;
			shape_index < (int)shape_count;
				++shape_index)
			{
				PxShape* curr_shape = shapes[shape_index];
				AddWidget(curr_shape, rigid_actor, geo_color);
			}

			delete[]shapes;
		}
	}

	delete[] actor_list;

	int articulation_count = physics_scene->getNbArticulations();

	for (int a = 0; a < articulation_count; ++a)
	{
		PxArticulation* articulation;
		physics_scene->getArticulations(&articulation, 1, a);

		int link_count = articulation->getNbLinks();

		PxArticulationLink** links = new PxArticulationLink*[link_count];
		articulation->getLinks(links, link_count);

		for (int l = 0; l < link_count; ++l)
		{
			PxArticulationLink* link = links[l];
			int shape_count = link->getNbShapes();

			for (int s = 0; s < shape_count; ++s)
			{
				PxShape* shape;
				link->getShapes(&shape, 1, s);
				AddWidget(shape, link, geo_color);
			}
		}
		delete[] links;
	}
}
