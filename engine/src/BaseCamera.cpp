#include "BaseCamera.h"

#include <glm\ext.hpp>

BaseCamera::BaseCamera() : BaseCamera(glm::mat4(1))
{

}

BaseCamera::BaseCamera(glm::vec3 position) : BaseCamera()
{
	worldTransform[3] = glm::vec4(position, 1);
	isPerspectiveSet = false;
}

BaseCamera::BaseCamera(glm::mat4 transform)
{
	SetWorldTransform(transform);
}

void BaseCamera::SetWorldTransform(glm::mat4 transform)
{
	worldTransform = transform;
	UpdateProjectionViewTransform();
}

const glm::mat4 BaseCamera::GetWorldTransform() const
{
	return worldTransform;
}

void BaseCamera::SetPosition(glm::vec3 position)
{
	worldTransform[3] = glm::vec4(position, 1);
	UpdateProjectionViewTransform();
}

void BaseCamera::SetLookAt(const glm::vec3& lookAt, const glm::vec3& up)
{
	worldTransform = glm::inverse(glm::lookAt(GetPosition(), lookAt, up));
	UpdateProjectionViewTransform();
}

void BaseCamera::SetLookAt(const glm::vec3& position, const glm::vec3& lookAt, const glm::vec3& up)
{
	worldTransform = glm::inverse(glm::lookAt(position, lookAt, up));
	UpdateProjectionViewTransform();
}

void BaseCamera::SetPerspective(float fieldOfView, float aspectRatio, float near, float far)
{
	projectionTransform = glm::perspective(fieldOfView, aspectRatio, near, far);
	isPerspectiveSet = true;

	UpdateProjectionViewTransform();
}

void BaseCamera::UpdateProjectionViewTransform()
{
	projectionViewTransform = projectionTransform * glm::inverse(worldTransform);
}