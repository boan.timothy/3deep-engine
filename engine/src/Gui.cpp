#include "Gui.h"

#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"
#include "glm\vec2.hpp"

#include "Window.h"

Gui::Gui(Window* window)
{
	Initialise(window);
}

Gui::~Gui()
{
	Shutdown();
}

bool Gui::Initialise(Window* window)
{
	ImGui_ImplGlfwGL3_Init(window->GetWindow(), true);

	ImGuiIO& io = ImGui::GetIO();

	glm::ivec2 windowSize = window->GetDisplaySize();
	io.DisplaySize.x = (float)windowSize.x;
	io.DisplaySize.y = (float)windowSize.y;
	
	return true;
}

void Gui::NewFrame()
{
	ImGui_ImplGlfwGL3_NewFrame();
}

void Gui::Render()
{
	ImGui::Render();
}

void Gui::Shutdown()
{
	ImGui_ImplGlfwGL3_Shutdown();
}