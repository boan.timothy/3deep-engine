#pragma once

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

#include <iostream>

namespace glen
{
	// Unsure as to whether this sort of thing should be a struct or class, but given we're putting
	// OpenGL debug stuff here, it'll be a namespace so it's technically "global"
	namespace Graphics
	{
		void APIENTRY openglCallbackFunction(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
		{
#ifdef SUPPRESS_GL_CALLBACKS_NONE
			if (severity != GL_DEBUG_SEVERITY_LOW ||
				severity != GL_DEBUG_SEVERITY_MEDIUM ||
				severity != GL_DEBUG_SEVERITY_HIGH)
			{
				//std::cout << "> Suppressed non-severe OpenGL callback" << std::endl;
				return;
			}
#endif

			std::cout << "== START OPENGL CALLBACK ======================================================" << std::endl;

			std::cout << "Type:\t\t";
			switch (type) {
			case GL_DEBUG_TYPE_ERROR:
				std::cout << "ERROR";
				break;
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				std::cout << "DEPRECATED_BEHAVIOR";
				break;
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				std::cout << "UNDEFINED_BEHAVIOR";
				break;
			case GL_DEBUG_TYPE_PORTABILITY:
				std::cout << "PORTABILITY";
				break;
			case GL_DEBUG_TYPE_PERFORMANCE:
				std::cout << "PERFORMANCE";
				break;
			case GL_DEBUG_TYPE_OTHER:
				std::cout << "OTHER";
				break;
			}
			std::cout << std::endl;

			std::cout << "ID:\t\t" << id << std::endl;

			std::cout << "Severity:\t";
			switch (severity) {
			case GL_DEBUG_SEVERITY_LOW:
				std::cout << "Low";
				break;
			case GL_DEBUG_SEVERITY_MEDIUM:
				std::cout << "Medium";
				break;
			case GL_DEBUG_SEVERITY_HIGH:
				std::cout << "High";
				break;
			default:
				std::cout << "None";
				break;
			}
			std::cout << std::endl;

			std::cout << message << std::endl;

			std::cout << "== END OPENGL CALLBACK ========================================================" << std::endl << std::endl;
		}
		
		void EnableDebugLogging()
		{
			if (glDebugMessageCallback == nullptr) return;
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageCallback(openglCallbackFunction, nullptr);
			GLuint unusedIds = 0;
			glDebugMessageControl(
				GL_DONT_CARE, // source
				GL_DONT_CARE, // type
				GL_DONT_CARE, // severity
				0,
				&unusedIds,
				true);
		}
	}
}