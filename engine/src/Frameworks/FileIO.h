#pragma once

#include <debugapi.h>
#include <string>
#include <iostream>

namespace glen
{
	struct FileIO
	{
		// Checks if the application was launched inside a debugger, and returns the path to 
		// the resource folder. Due to the way Visual Studio handles working directories, the
		// resources folder is only three directories above the working directory, while running
		// the application from the exe means the resources folder actually is four directories up.
		static std::string GetResourcePath()
		{
			if (IsDebuggerPresent())
				return "../../../resources/";

			return "../../../../resources/";
		}
	};
}