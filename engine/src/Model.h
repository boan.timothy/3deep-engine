#pragma once

#include <glm\vec3.hpp>
#include <vector>
#include <string>

class FBXFile;
class BaseCamera;

namespace glen
{
	class Texture;
	class Shader;

	// This class is specifically designed to handle FBX models. It's very tightly coupled with the lighting solution
	// set out in the "fbx_textured.frag" shader for this particular assessment due to time constraints.
	// Ultimately, the goal would be to take the lighting out of this class and put it into another one and have all
	// FBXModels that are lit be influenced by lights in the scene.
	class FBXModel
	{
	public:
		FBXModel() = delete;
		FBXModel(std::string pathToFBXModel);
		~FBXModel();

		void Render(BaseCamera* camera);

		void SetLightDirection(glm::vec3 dir) { lightDirection = dir; }
		void SetLightColor(glm::vec4 color) { lightColor = color; }
		void SetSpecularPower(float power) { specularPower = power; }

		glm::vec3 GetLightDirection() { return lightDirection; }
		glm::vec4 GetLightColor() { return lightColor; }
		float GetSpecularPower() { return specularPower; }

	private:
		void GenerateBuffers(FBXFile* fbx);

		glm::vec3 lightDirection;
		glm::vec4 lightColor;
		float specularPower;

		FBXFile* fbx;
		Shader* shader;
		std::vector<Texture*> textures;
	};
}