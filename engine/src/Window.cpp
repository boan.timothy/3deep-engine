#include "Window.h"

#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

#include <iostream>

Window::Window()
{
	window = nullptr;
}

Window::~Window()
{
	if (window != nullptr)
		DestroyWindow();
}

Window* Window::InitialiseWindow(const char* title, VideoMode vm, unsigned int width, unsigned int height)
{	
	//Window* win = new Window;
	
	// Video mode is set to Windowed and a valid width and height has been supplied
	if (vm == VideoMode::Windowed && width > 0 && height > 0)
	{
		//win->
		window = glfwCreateWindow(width, height, title, nullptr, nullptr);
	}

	// Video mode is set to Fullscreen and a width and height has been supplied
	else if (vm == VideoMode::Fullscreen && width > 0 && height > 0)
	{
		GLFWmonitor* mon = glfwGetPrimaryMonitor();
		if (mon == nullptr)
		{
			std::cout << "Window::InitialiseWindow() - glfwGetPrimaryMonitor() failed." << std::endl;
			return nullptr;
		}

		//win->
		window = glfwCreateWindow(width, height, title, mon, nullptr);
	}

	// Video mode is set to Fullscreen and we will use the default resolution
	// of the primary monitor
	else if (vm == VideoMode::Fullscreen)
	{
		GLFWmonitor* mon = glfwGetPrimaryMonitor();
		if (mon == nullptr)
		{
			std::cout << "Window::InitialiseWindow() - glfwGetPrimaryMonitor() failed." << std::endl;
			return nullptr;
		}

		GLFWvidmode* mode = (GLFWvidmode*)glfwGetVideoMode(mon);

		if (mode == nullptr)
		{
			std::cout << "Window::InitialiseWindow() - glfwGetVideoMode() failed." << std::endl;
			return nullptr;
		}

		//win->
		window = glfwCreateWindow(mode->width, mode->height, title, mon, nullptr);
	}

	else // Windowed mode had invalid width/height parameters
	{
		std::cout << "Window::InitialiseWindow() - Invalid parameters used during function call." << std::endl;
		return nullptr;
	}


	if (//win->
		window == nullptr)
	{
		std::cout << "Window::InitialiseWindow() - Failed to create GLFW window." << std::endl;
		glfwTerminate();
		return nullptr;
	}

	return this;
}

void Window::DestroyWindow()
{
	glfwDestroyWindow(window);
	window = nullptr;
}

GLFWwindow* Window::GetWindow()
{
	return window;
}

glm::ivec2 Window::GetDisplaySize()
{
	glm::ivec2 ws;
	glfwGetWindowSize(window, &ws.x, &ws.y);
	return ws;
}