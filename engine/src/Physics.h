#pragma once

#include <PxPhysicsAPI.h>
#include <PxScene.h>
#include <pvd\PxVisualDebugger.h>

using namespace physx;

class ParticleEmitter;
class ParticleFluidEmitter;

// Custom PhysX memory allocator. This doesn't actually get used by the Physics class, but it's
// a drop in replacement for the standard allocator PhysX provides.
class PhysicsAllocator : public PxAllocatorCallback
{
public:
	virtual ~PhysicsAllocator() {}
	virtual void* allocate(size_t size, const char* typeName, const char* filename, int line)
	{
		void* pointer = _aligned_malloc(size, 16);
		return pointer;
	}
	virtual void deallocate(void* ptr)
	{
		_aligned_free(ptr);
	}
};

// Custom overriding hit report class used by a player controller.
class ControllerHitReport : public PxUserControllerHitReport
{
public:
	virtual void onShapeHit(const PxControllerShapeHit &hit)
	{
		PxRigidActor* actor = hit.shape->getActor();
		_playerContactNormal = hit.worldNormal;
		PxRigidDynamic* myActor = actor->is<PxRigidDynamic>();
		if (myActor)
		{

		}
	};
	virtual void onControllerHit(const PxControllersHit &hit) {}
	virtual void onObstacleHit(const PxControllerObstacleHit &hit) {}

	ControllerHitReport() : PxUserControllerHitReport() {}
	PxVec3 getPlayerContactNormal() { return _playerContactNormal; }
	void clearPlayerContactNormal() { _playerContactNormal = PxVec3(0, 0, 0); }
	PxVec3 _playerContactNormal;
};

namespace glen
{
	// This Physics class is the primary method of interacting with PhysX.
	// It's not particularly neat or well coded, as it was put together it a rush in a desparate
	// (and foolish) attempt to make the assessment deadline. There's plenty of functions and variables
	// that have been tweaked and changed and left in as comments due to the changing nature of how
	// this class functioned internally.
	class Physics
	{
	public:
		Physics() {};
		~Physics() {};

		void Startup();
		void Update(float deltaTime, GLFWwindow* window);
		void Shutdown();
		
		PxRigidStatic* CreateHeightMap(int rowCount, int columnCount, void* sampleData, int heightScale);

		void SetUpVisualDebugger();
		void SetUpDynamicObjects();
		void SetUpPlayerController();
		void SetUpFluidDynamics();

		PxScene* GetScene() { return scene; }

		void RenderGizmos(PxScene* physics_scene);
		//void AddWidget(PxShape* shape, PxRigidActor* actor, glm::vec4 geo_color);

	private:

		void UpdatePlayerController(float deltaTime, GLFWwindow* window);
		void UpdateFluidDynamics(float deltaTime);

		PxFoundation*				foundation;
		PxPhysics*					physics;
		PxScene*					scene;
		PxDefaultErrorCallback		defaultErrorCallback;
		PxDefaultAllocator			defaultAllocatorCallback;
		PxSimulationFilterShader	defaultFilterShader = PxDefaultSimulationFilterShader;
		PxMaterial*					material;
		PxDefaultCpuDispatcher*		cpuDispatcher;
		//PxMaterial*					boxMaterial;
		//PxCooking*					cooker;

		PxController*				playerController;
		PxControllerManager*		controllerManager;
		ControllerHitReport*		hitReport;

		PxVisualDebuggerConnection* connection;

		//PxHeightField*				heightField;

		PxParticleFluid*			particleSystem;
		ParticleFluidEmitter*		particleEmitter;

		float velocity;
		float rotation;
		float gravity;
	};
}