#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <glm\ext.hpp>
#include <FBXFile.h>

#include "Frameworks/FileIO.h"
#include "BaseCamera.h"
#include "Texture.h"
#include "Shader.h"

#include "Model.h"

using namespace glen;

FBXModel::FBXModel(std::string pathToFBXModel)
{
	// Load FBX Model, retrieving the correct path
	fbx = new FBXFile();
	std::string path = FileIO::GetResourcePath() + pathToFBXModel;
	std::cout << "Loading a model - this could take a while!" << std::endl;
	std::cout << "If you have vld.h included, it could be more than a minute on some systems." << std::endl;
	if (fbx->load(path.c_str()))
	{

		// Load in the textures associated with the FBX model, if any
		for (unsigned int i = 0; i < fbx->getTextureCount(); i++)
		{
			textures.emplace_back(new glen::Texture(fbx->getTextureByIndex(i)->path, true));
		}

		// Load in and link the correct shader files for a textured FBX
		shader = new Shader();
		shader->LoadVertexShader("shaders/textured_fbx.vert");
		shader->LoadFragmentShader("shaders/textured_fbx.frag");
		shader->LinkShaders();

		lightColor = glm::vec4(1, 1, 1, 1);
		lightDirection = glm::vec3(0, 1, -1);
		specularPower = 4.0f;

		// Generate the OpenGL buffers for this FBX model
		GenerateBuffers(fbx);
	}
	else
	{
		std::cout << "ERROR: [MODEL] FBX MODEL FILE NOT SUCCESSFULLY READ: " << pathToFBXModel << std::endl;
	}
}

FBXModel::~FBXModel()
{
	// Destroy OpenGL buffers
	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);

		unsigned int* data = (unsigned int*)mesh->m_userData;

		glDeleteVertexArrays(1, &data[0]);
		glDeleteVertexArrays(1, &data[1]);
		glDeleteVertexArrays(1, &data[2]);

		delete[] data;
	}

	// Destroy textures
	for (unsigned int i = 0; i < textures.size(); i++)
	{
		delete textures[i];
		textures[i] = nullptr;
	}
	textures.clear();

	delete shader;
	shader = nullptr;

	delete fbx;
	fbx = nullptr;
}

void FBXModel::Render(BaseCamera* camera)
{
	unsigned int program = shader->GetShaderProgram();
	glUseProgram(program);

	unsigned int uniform = glGetUniformLocation(program, "projectionView");
	glUniformMatrix4fv(uniform, 1, GL_FALSE, glm::value_ptr(camera->GetProjectionView()));

	uniform = glGetUniformLocation(program, "cameraPosition");
	glUniform3fv(uniform, 1, glm::value_ptr(camera->GetPosition()));

	uniform = glGetUniformLocation(program, "lightDirection");
	glUniform3fv(uniform, 1, glm::value_ptr(lightDirection));

	uniform = glGetUniformLocation(program, "lightColor");
	glUniform4fv(uniform, 1, glm::value_ptr(lightColor));

	uniform = glGetUniformLocation(program, "specularPower");
	glUniform1f(uniform, specularPower);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[0]->GetTexture());
	uniform = glGetUniformLocation(program, "diffuse");
	glUniform1i(uniform, 0);

	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{

		FBXMeshNode* mesh = fbx->getMeshByIndex(i);
		unsigned int* data = static_cast<unsigned int*>(mesh->m_userData);

		glBindVertexArray(data[0]);
		glDrawElements(GL_TRIANGLES, (unsigned int)mesh->m_indices.size(), GL_UNSIGNED_INT, 0);
	}
}

void FBXModel::GenerateBuffers(FBXFile* fbx)
{
	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);

		unsigned int* glData = new unsigned int[3];

		glGenVertexArrays(1, &glData[0]);
		glBindVertexArray(glData[0]);

		glGenBuffers(1, &glData[1]);
		glGenBuffers(1, &glData[2]);

		glBindBuffer(GL_ARRAY_BUFFER, glData[1]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glData[2]);

		glBufferData(GL_ARRAY_BUFFER,
			mesh->m_vertices.size() * sizeof(FBXVertex),
			mesh->m_vertices.data(), GL_STATIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			mesh->m_indices.size() * sizeof(unsigned int),
			mesh->m_indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); // Position
		glEnableVertexAttribArray(1); // Color
		glEnableVertexAttribArray(2); // Normal
		glEnableVertexAttribArray(3); // Tangent
		glEnableVertexAttribArray(4); // BiNormal
		glEnableVertexAttribArray(5); // Indices
		glEnableVertexAttribArray(6); // Weights
		glEnableVertexAttribArray(7); // TexCoord1
		glEnableVertexAttribArray(8); // TexCoord2

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), 0);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)Offsets1::ColourOffset);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_TRUE,  sizeof(FBXVertex), (void*)Offsets1::NormalOffset);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)Offsets1::TangentOffset);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)Offsets1::BiNormalOffset);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)Offsets1::IndicesOffset);
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)Offsets1::WeightsOffset);
		glVertexAttribPointer(7, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)Offsets1::TexCoord1Offset);
		glVertexAttribPointer(8, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)Offsets1::TexCoord2Offset);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		mesh->m_userData = glData;
		int e = 0;
	}
}